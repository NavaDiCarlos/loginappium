package com.Pages;

import org.openqa.selenium.By;
import com.Utils.Util;


public class LoginPage extends Hooks {

	Util _Util	;

	By txtEmail = By.xpath(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.EditText[1]");
	By txtPassword = By.xpath(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.EditText[2]");
	By btnSignin = By.xpath(
	"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.Button");

	By txtView = By.xpath(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView");
			
	

	public void Login_process_with_valid_email_and_password(String email, String password) throws Exception {

		_Util	= new Util();
		if (!_Util.Validate_Email(email))
			throw new Exception("Test Failed: Not a valid username, please insert a valid username.");

		if (password.length() < 5)
			throw new Exception("Test Failed: Password must be >5 characters.");
		
		if(driver.findElement(btnSignin).isEnabled())
			throw new Exception("Test Failed: The sign in button is not disabled.");
		
		driver.findElement(txtEmail).sendKeys(email);
		driver.findElement(txtPassword).sendKeys(password);
		_Util.ThreadSleep(2);
		driver.findElement(btnSignin).click();
		_Util.ThreadSleep(2);
		
		if(!driver.findElement(txtView).getText().replace("Hello ", "").equals(email))
			throw new Exception("Test Failed: The sign in button is not disabled.");
		
		
	}

}
