package com.Pages;

import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.appium.java_client.remote.MobileCapabilityType;

public class Hooks {
static WebDriver driver;
	
	public void init() {
		try {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability(CapabilityType.PLATFORM_NAME, "Android");
			caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8");
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, "generic_x86");
			caps.setCapability(MobileCapabilityType.UDID, "emulator-5554");
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
			caps.setCapability("appPackage", "io.grainchain.logintest");
			caps.setCapability("appActivity", "io.grainchain.logintest.ui.login.LoginActivity");
			caps.setCapability("noReset", true);
			caps.setCapability("noSign", true);
			caps.setCapability("autoGrantPermissions", true);
			URL url = new URL("http://127.0.0.1:4723/wd/hub");
			driver = new RemoteWebDriver(url, caps);
			
		} catch (Exception ex) {
			System.out.println("Error: " + ex);
		}
	}
	
	public void tearDown() 
	{
		driver.quit();
	}
}
