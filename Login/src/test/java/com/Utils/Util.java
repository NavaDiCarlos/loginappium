package com.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Util  {

	public boolean Validate_Email(String email) {
		
		  Pattern pattern = Pattern
	                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			
	        Matcher mather = pattern.matcher(email);
	 
	        if (mather.find() == true) {
	           return true;
	        } return false;
	}
	
	public void ThreadSleep(int segundos) {
		try {
			Thread.sleep(segundos * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	

}
