package com.StepsDefinition;

import com.Pages.LoginPage;

import io.cucumber.java.en.Given;

public class LoginStepDefinition {

	LoginPage _LoginPage = new LoginPage();

	@Given("^Automate the login process with valid email (.*) and password (.*)$")
	public void Automate_the_login_process_with_valid_email_and_password(String email, String password) {
		try {
			
			_LoginPage.Login_process_with_valid_email_and_password(email, password);

		} catch (Exception ex) {
			System.out.println("Error in test " + ex);
		}

	}

}
