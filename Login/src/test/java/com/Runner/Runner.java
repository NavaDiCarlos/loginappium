package com.Runner;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.Pages.Hooks;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(features = "src/test/resources/Features/CDPLogin.feature", tags = { "@tag1" }, glue = {
		"com.StepsDefinition" }, monochrome = true,strict = true)
public class Runner {
	static Hooks hooks = new Hooks();

	@BeforeClass
	public static void BeforeScenarios() {

		hooks.init();
	}

	@AfterClass
	public static void AfterScenarios() {
		hooks.tearDown();
	}

}

