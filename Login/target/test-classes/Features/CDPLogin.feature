#Author: navadicarlos@gmail.com

Feature: Scenario Login 
 
  @tag1
  Scenario Outline: Validate login

  Given Automate the login process with valid email <email> and password <password>
  
    Examples: 
      | email  | password |
      | navadicarlos@gmail.com | navadicarlos |